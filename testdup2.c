#include "user.h"
#include "fcntl.h"
#include "types.h"

int
main (void)
{
   int fd;
   int fd2;

   //Prueba de fichero cerrado
   dup2(1,5);

   printf(5, "Prueba de duplicación de salida estándar\n");

   //Prueba de duplicación de fichero ya abierto
   dup2(1,2);
   printf(2, "Prueba de duplicación de salida estándar\n");

   fd = open("test", 0_RDWR);
   dup2(1,fd);
   printf(fd,"Prueba de duplicación de salida estándar\n");

   close(fd);

   return 0;
}
